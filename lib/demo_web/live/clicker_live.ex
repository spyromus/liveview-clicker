defmodule DemoWeb.ClickerLive do
  use Phoenix.LiveView

  def render(assigns) do
    ~L"""
    <label>Нажатий: <%= @clicks %></label>
    <button phx-click="click">Нажми меня</button>
    """
  end

  def mount(_session, socket) do
    socket =
      socket
      |> assign(:clicks, Demo.Clicker.value())

    if connected?(socket) do
      Demo.Clicker.subscribe()
    end

    {:ok, socket}
  end
  
  def handle_event("click", _, socket) do
    Demo.Clicker.increment()
    {:noreply, socket}
  end

  def handle_info(:clicker_change, socket) do
    {:noreply, assign(socket, :clicks, Demo.Clicker.value)}
  end

end
