defmodule Demo.Clicker do
  use Agent

  def start_link(_opts) do
    Agent.start_link(fn -> 0 end, name: __MODULE__)
  end
  
  def value do
    Agent.get(__MODULE__, &(&1))
  end

  def increment do
    Agent.update(__MODULE__, &(&1 + 1))
    notify_change()
  end

  def reset do
    Agent.update(__MODULE__, fn -> 0 end)
    notify_change()
  end

  def subscribe do
    Phoenix.PubSub.subscribe(Demo.PubSub, "clicker")
  end

  def notify_change do
    Phoenix.PubSub.broadcast(Demo.PubSub, "clicker", :clicker_change)
  end
end
